# Josm style traffic signs orientation

This style rotate icon for nodes with highway=stop/give_way depending on the direction tag value.

If there is no direction tag and no oneway value on the parent highway, the icon is grey and rotated to forward direction.

To use it, add this link to in the Josm style settings : https://gitlab.com/cartocite/josm-style-traffic-signs-orientation/-/raw/main/traffic_sign_orientation.mapcss?ref_type=heads

![examples](examples.png)